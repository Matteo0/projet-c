#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "orchestre_service.h"
#include "client_service.h"
#include "../UTILS/myassert.h"
#include "service_compression.h"

// définition éventuelle de types pour stocker les données


/*----------------------------------------------*
 * fonctions appelables par le service
 *----------------------------------------------*/

// fonction de réception des données
static void receiveData(char *nomTube, char *str)
{
    int fd = open(nomTube, O_RDONLY);
    int ret = read(fd, str, sizeof(char) * (strlen(str) + 1));
    myassert(ret, "erreur lecture tube nommé");
    close(fd);
}

// fonction de traitement des données

static void computeResult(char *str, char *result)
{
    char nextLetter = str[1];
    
    int iNewTab = 0;
    int nbSameValue = 1;
    
    for(int i = 1; i <= strlen(str); i++) {
        if(nextLetter == str[i - 1]) {
            nbSameValue++;
        } else {
            result = realloc(result, sizeof(char) * (iNewTab + 2 + 1));
            result[iNewTab] = nbSameValue + '0';
            result[iNewTab + 1] = str[i -1];
            nbSameValue = 1;
            iNewTab += 2;
        }
        
        nextLetter = str[i + 1];
    }
    result[iNewTab] = '\0';
}


// fonction d'envoi du résultat
static void sendResult(char *nomTube, char *str)
{
    int fd = open(nomTube, O_WRONLY);
    int ret = write(fd, str, sizeof(char) * (strlen(str) + 1));
    myassert(ret, "erreur lecture tube nommé");
    close(fd);
}


/*----------------------------------------------*
 * fonction appelable par le main
 *----------------------------------------------*/
``` 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "orchestre_service.h"
#include "client_service.h"
#include "../UTILS/myassert.h"
#include "service_compression.h"

// définition éventuelle de types pour stocker les données


/*----------------------------------------------*
 * fonctions appelables par le service
 *----------------------------------------------*/

// fonction de réception des données
static void receiveData(char *nomTube, char *str)
{
    int fd = open(nomTube, O_RDONLY);
    int ret = read(fd, str, sizeof(char) * (strlen(str) + 1));
    myassert(ret, "erreur lecture tube nommé");
    close(fd);
}

// fonction de traitement des données

static void computeResult(char *str, char *result)
{
    char nextLetter = str[1];
    
    int iNewTab = 0;
    int nbSameValue = 1;
    
    for(int i = 1; i <= strlen(str); i++) {
        if(nextLetter == str[i - 1]) {
            nbSameValue++;
        } else {
            result = realloc(result, sizeof(char) * (iNewTab + 2 + 1));
            result[iNewTab] = nbSameValue + '0';
            result[iNewTab + 1] = str[i -1];
            nbSameValue = 1;
            iNewTab += 2;
        }
        
        nextLetter = str[i + 1];
    }
    result[iNewTab] = '\0';
}


// fonction d'envoi du résultat
static void sendResult(char *nomTube, char *str)
{
    int fd = open(nomTube, O_WRONLY);
    int ret = write(fd, str, sizeof(char) * (strlen(str) + 1));
    myassert(ret, "erreur lecture tube nommé");
    close(fd);
}


/*----------------------------------------------*
 * fonction appelable par le main
 *----------------------------------------------*/
void service_compression(char *nomTube)
{
    // initialisations diverses
    char *str = NULL;
    receiveData(nomTube, str);
    char *result = NULL;
    computeResult(str, result);
    sendResult(nomTube, result);
    // libération éventuelle de ressources
    free(str);
    free(result);
}
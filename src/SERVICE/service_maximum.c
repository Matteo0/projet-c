#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "orchestre_service.h"
#include "client_service.h"

#include "service_maximum.h"

// définition éventuelle de types pour stocker les données


/*----------------------------------------------*
 * fonctions appelables par le service
 *----------------------------------------------*/

// fonction de réception des données
void receiveData(/* fd_pipe_from_client, */ /* données à récupérer */)
{
}

// fonction de traitement des données
void computeResult(/* données récupérées, */ /* résultat */)
{
}

void service_somme(const char *recive, const char *send) /* fd tubes nommés avec un client */
{
    // initialisations diverses
    

	float resul;
	float donnees1,donnees2;
    receiveData(recive,&donnees1,&donnees2);/* paramètres */
	sleep(1);
    computeResult(donnees1,donnees2,&resul);
    sendResult(send,resul);
	
    // libération éventuelle de ressources
	rendreSemOrSer(fdSemSerSomme()); // fdSemSerSomme pour avoir l'id du semaphore service somme
}


// fonction d'envoi du résultat
void sendResult(/* fd_pipe_to_client,*/ /* résultat */)
{
}


/*----------------------------------------------*
 * fonction appelable par le main
 *----------------------------------------------*/
void service_maximum(const char *recive, const char *send)
{
    // initialisations diverses
    
    receiveData(/* paramètres */);
    computeResult(/* paramètres */);
    sendResult(/* paramètres */);

    // libération éventuelle de ressources
}

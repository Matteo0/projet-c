#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "orchestre_service.h"
#include "client_service.h"
#include "service.h"
#include "service_somme.h"
#include "service_compression.h"
#include "service_maximum.h"


static void usage(const char *exeName, const char *message)
{
    fprintf(stderr, "usage : %s <num_service> <clé_sémaphore> <fd_tube_anonyme> "
            "<nom_tube_service_vers_client> <nom_tube_client_vers_service>\n",
            exeName);
    fprintf(stderr, "        <num_service>     : entre 0 et %d\n", SERVICE_NB - 1);
    fprintf(stderr, "        <clé_sémaphore>   : entre ce service et l'orchestre (clé au sens ftok)\n");
    fprintf(stderr, "        <fd_tube_anonyme> : entre ce service et l'orchestre\n");
    fprintf(stderr, "        <nom_tube_...>    : noms des deux tubes nommés reliés à ce service\n");
    if (message != NULL)
        fprintf(stderr, "message : %s\n", message);
    exit(EXIT_FAILURE);
}

//float codethrea


/*----------------------------------------------*
 * fonction main
 *----------------------------------------------*/
int main(int argc, char * argv[])
{
    if (argc != 6)
        usage(argv[0], "nombre paramètres incorrect");

    // initialisations diverses : analyse de argv
	
	bool etat = true;
	

    while (etat)
    {
        // attente d'un code de l'orchestre (via tube anonyme)
        int fdPass,size,buf;
		fdPass = open(argv[3],O_RDONLY,0644);
		myassert(fdPass != -1, "fichier non ouvert");
		size = read(fdPass,&buf,sizeof(int));
		myassert( size != -1, "fichier non lu" );
		// si code de fin
        //    sortie de la boucle
		// liberation de l'orchestre pour qu'il s'occupe d'un autre client
		rendreSemOrSer(fdSemOrSer());
		if(buf == -1){
			close(fdPass);
			break;
		}
        // sinon
        //    réception du mot de passe de l'orchestre
		int passworldOrc,passworldCli;
		size = read(fdPass,&passworldOrc,sizeof(int));
		myassert(size != -1, "Read pas fonctionnelle");
		
        //    ouverture des deux tubes nommés avec le client
		int fdSerCli = open(argv[4],O_WRONLY,0644);
		myassert(fdSerCli != -1,"open pas fonctionnelle");
		int	fdCliSer = open(argv[5],O_RDONLY,0644);
		myassert(fdCliSer != -1, "open pas fonctionnelle");
		
        //    attente du mot de passe du client
		sleep(1);
		size = read(fdCliSer,&passworldCli,sizeof(int));
		myassert(size != -1,"Read pas fonctionnelle");
		
        //    si mot de passe incorrect
		//        envoi au client d'un code d'erreur
		if(passworldCli != passworldOrc){
			const char *code = "Mot de passe incorrect ";
			write(fdSerCli,code, sizeof(char)*strlen(code));
			close(fdPass);
			close(fdSerCli);
			close(fdCliSer);
			break;
		}
        //    sinon
        //        envoi au client d'un code d'acceptation
		const char *code = "Mot de passe correct";
		write(fdSerCli,code, (sizeof(char)*strlen(code)));
		
		//        appel de la fonction de communication avec le client :
        //            une fct par service selon numService (cf. argv[1]) :
        //                   . service_somme
        //                ou . service_compression
        //                ou . service_maximum
		if((*argv[1] != 0) || (*argv[1] != 1) || (*argv[3] != 2)){
			const char *msg = "Code service incorrecte. Veuillez recommencez SVP!!!!! ";
			write(fdSerCli,msg, sizeof(char)*strlen(msg));
			close(fdPass);
			close(fdSerCli);
			close(fdCliSer);
			break;
		}		
		if(*argv[1] == 0){
			prendreSemOrSer(fdSemSerSomme());
			service_somme(argv[5],argv[4]);		
		}else{
			if(*argv[1] == 2){
				prendreSemOrSer(fdSemSerMax());
				service_maximum(argv[5],argv[4]);
			}else{
				prendreSemOrSer(fdSemSerComp());
				service_compression(argv[5],argv[4]);
			}
		}
		
        //        attente de l'accusé de réception du client
		size = read(fdCliSer,&passworldCli,sizeof(int));
		myassert(size != -1,"Read pas fonctionnelle");
		if(passworldCli!=-1){
			sleep(1);
		}
        //    finsi
        //    fermeture ici des deux tubes nommés avec le client
		close(fdCliSer);
		close(fdSerCli);
        //    modification du sémaphore pour prévenir l'orchestre de la fin deja fait depuis chaque service

        // finsi
    }

    
    return EXIT_SUCCESS;
}

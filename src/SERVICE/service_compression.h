#ifndef SERVICE_COMPRESSION_H
#define SERVICE_COMPRESSION_H

// on ne déclare ici que les fonctions appelables par le main

// fonction pour gérer la communication avec le client
static void receiveData(char *nomTube, char *str);
static void computeResult(char *str, char *result);
static void sendResult(char *nomTube, char *str);
void service_compression(char *nomTube);

#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "orchestre_service.h"
#include "client_service.h"

#include "service_somme.h"

// définition éventuelle de types pour stocker les données


/*----------------------------------------------*
 * fonctions appelables par le service
 *----------------------------------------------*/

// fonction de réception des données
static void receiveData(const char *pipeSerCli,float *donnees1,float *donnees2) /* fd_pipe_from_client, */ /* données à récupérer */
{

	int	fdCliSom = open(pipeSerCli,O_RDONLY,0644);
	myassert(fdCliSom != -1, "fichier non ouvert");
	int fdRead;
	fdRead = read(fdCliSom,donnees1,sizeof(float));
	myassert(fdRead != -1, " fichier non lu");
	fdRead = read(fdCliSom,donnees2,sizeof(float));
	myassert(fdRead != -1,"fichier non lu");
	close(fdCliSom);

}

// fonction de traitement des données
static void computeResult(float val1,float val2,float *resultat)
{
	*resultat = (val1 + val2);
}

// fonction d'envoi du résultat
static void sendResult(const char *send,float result)/* fd_pipe_to_client,*/ /* résultat */
{
	int fdSomCli = open(send,O_WRONLY,0644);
	myassert(fdSomCli != -1, "fichier non ouvert");
	write(fdSomCli,&result, sizeof(char));
	close(fdSomCli);
}


/*----------------------------------------------*
 * fonction appelable par le main
 *----------------------------------------------*/
void service_somme(const char *recive, const char *send) /* fd tubes nommés avec un client */
{
    // initialisations diverses
    

	float resul;
	float donnees1,donnees2;
    receiveData(recive,&donnees1,&donnees2);/* paramètres */
	sleep(1);
    computeResult(donnees1,donnees2,&resul);
    sendResult(send,resul);
	
    // libération éventuelle de ressources
	rendreSemOrSer(fdSemSerSomme()); // fdSemSerSomme pour avoir l'id du semaphore service somme
}

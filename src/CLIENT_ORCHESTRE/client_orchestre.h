#ifndef CLIENT_ORCHESTRE_H
#define CLIENT_ORCHESTRE_H
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <string.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>


#define MON_FICHIER "client_orchestre.h"
#define PROJ_IDORCLI 93
// Ici toutes les communications entre l'orchestre et les clients :
// - le sémaphore pour que 2 clients ne conversent pas en même
//   temps avec l'orchestre
// - les deux tubes nommés pour la communication bidirectionnelle

typedef struct{
	
	char *O2C;
	char *C2O;
}TubeN;

int creerSemOrCli();

void detruireSemOrCli(int semId);

int fdSemOrCli();

void prendreSemOrCli(int semId);

void rendreSemOrCli(int semId);

TubeN creationDeuxtubeNomme(const char *C2O,const char *O2C);

void detriuretubes();

#endif

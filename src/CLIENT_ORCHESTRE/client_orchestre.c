#include "myassert.h"

#include "client_orchestre.h"


// Creation d'un semaphore(Entrer et sortie zone critique)
int creerSemOrCli()
{
    key_t key;
    int semId;
    int ret;

    key = ftok(MON_FICHIER, PROJ_IDORCLI); // PROJ_IDORCLI est un macro afin de creer une cle de com unique entre orchestre et client
    myassert(key != -1,"ftok non ouvert");
    semId = semget(key, 1, IPC_CREAT | IPC_EXCL | 0641);
    myassert(semId != -1,"Semaphore non creer");
    ret = semctl(semId, 0, SETVAL,1);
    myassert(ret != -1,"Semaphore non initialiser");

    return semId;
}


// Destruction du sémaphore
void detruireSemOrCli(int semId)
{
    int ret;
    ret = semctl(semId, -1, IPC_RMID);
    myassert(ret != -1, "Semaphore non Detruit");
}

// Retouner la l'identifiant du semaphore
int fdSemOrCli()
{
    key_t key;
    int semId;

    key = ftok(MON_FICHIER, PROJ_IDORCLI);
    myassert(key != -1, "Fichier compromis");
    semId = semget(key, 1, 0);
    myassert(semId != -1, "Valeur du semaphore non prise en compte");
    return semId;
}

// Prendre une resource(entrer dans une zone critique)
void prendreSemOrCli(int semId)
{
    int ret;
    struct sembuf operationMoins = {0, -1, 0};
    ret = semop(semId, &operationMoins, 1);
    myassert(ret != -1,"Operation non prise en compte, imposible de rendre la resource");
}


// Rendre une resource(sortir d'une zone critique)	
void rendreSemOrCli(int semId)
{
    int ret;
    struct sembuf operationPlus = {0, 1, 0};
    ret = semop(semId, &operationPlus, 1);
    myassert(ret != -1,"Operation non prise en compte, imposible de donner la resource");
}


// Creation de 2 tube nommé entre orchestre et service(puis client et service)
TubeN creationDeuxtubeNomme(const char *C2O,const char *O2C){
    int ret, taille1,taille2; 
    taille1 = strlen(C2O);
    taille2 = strlen(O2C);
    TubeN tube;
    tube.C2O = malloc(sizeof(char)*(1+taille1));
    tube.O2C = malloc(sizeof(char)*(1+taille2));
    sprintf(tube.C2O,"%s",C2O);
    sprintf(tube.O2C,"%s",O2C);
    ret = mkfifo( O2C,0641);
    myassert(ret!=-1,"Pipe non creer");
    ret = mkfifo(C2O,0641);
    myassert(ret!=-1,"Pipe non creer");
}

void detriuretubes(){

	
}







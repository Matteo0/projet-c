#include "myassert.h"

#include "orchestre_service.h"

// Creation d'un semaphore(Entrer et sortie zone critique)
int creerSemOrSer()
{
    key_t key;
    int semId;
    int ret;

    key = ftok(MON_FICHIER, PROJ_IDORSER);
    myassert(key != -1,"ftok non ouvert");
    semId = semget(key, 1, IPC_CREAT | IPC_EXCL | 0641);
    myassert(semId != -1,"Semaphore non creer");
    ret = semctl(semId, 0, SETVAL,1);
    myassert(ret != -1,"Semaphore non initialiser");

    return semId;
}


// Destruction du sémaphore
void detruireSemOrSer(int semId)
{
    int ret;
    ret = semctl(semId, -1, IPC_RMID);
    myassert(ret != -1, "Semaphore non Detruit");
}

// Retouner la l'identifiant du semaphore
int fdSemOrSer()
{
    key_t key;
    int semId;

    key = ftok(MON_FICHIER, PROJ_IDORSER);
    myassert(key != -1, "Fichier compromis");
    semId = semget(key, 1, 0);
    myassert(semId != -1, "Valeur du semaphore non prise en compte");
    return semId;
}

// Prendre une resource(entrer dans une zone critique)
void prendreSemOrSer(int semId)
{
    int ret;
    struct sembuf operationMoins = {0, -1, 0};
    ret = semop(semId, &operationMoins, 1);
    myassert(ret != -1,"Operation non prise en compte, imposible de rendre la resource");
}

// Rendre une resource(sortir d'une zone critique)	

void rendreSemOrSer(int semId)
{
    int ret;
    struct sembuf operationPlus = {0, 1, 0};
    ret = semop(semId, &operationPlus, 1);
    myassert(ret != -1,"Operation non prise en compte, imposible de donner la resource");
}

// Creation pipe entre orchestre et service !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void creationPipeOrSer(int *pip){
	int ret;
	ret = pipe(pip);
	myassert(ret!=-1,"Pipe non creer");
}


// Semaphore pour que chaque service ne soit pas contacter par deux clients en meme temps
int creerSemSerOcuper(int numSer)
{
    key_t key;
    int semId;
    int ret;

    key = ftok(MON_FICHIER, 1+numSer);
    myassert(key != -1,"ftok non ouvert");
    semId = semget(key, 1, IPC_CREAT | IPC_EXCL | 0641);
    myassert(semId != -1,"Semaphore non creer");
    ret = semctl(semId, 0, SETVAL,1);
    myassert(ret != -1,"Semaphore non initialiser");

    return semId;
}

// id service sommm
int fdSemSerSomme()
{
    key_t key;
    int semId;

    key = ftok(MON_FICHIER, 1);
    myassert(key != -1, "Fichier compromis");
    semId = semget(key, 1, 0);
    myassert(semId != -1, "Valeur du semaphore non prise en compte");
    return semId;
}

// id service max
int fdSemSerMax()
{
    key_t key;
    int semId;

    key = ftok(MON_FICHIER, 3);
    myassert(key != -1, "Fichier compromis");
    semId = semget(key, 1, 0);
    myassert(semId != -1, "Valeur du semaphore non prise en compte");
    return semId;
}

// id Service compression
int fdSemSerComp()
{
    key_t key;
    int semId;

    key = ftok(MON_FICHIER, 2);
    myassert(key != -1, "Fichier compromis");
    semId = semget(key, 1, 0);
    myassert(semId != -1, "Valeur du semaphore non prise en compte");
    return semId;
}



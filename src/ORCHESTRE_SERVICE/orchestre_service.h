#ifndef ORCHESTRE_SERVICE_H
#define ORCHESTRE_SERVICE_H
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <semaphore.h>

#define MON_FICHIER "orchestre_service.h"
#define PROJ_IDORSER 99

// Ici toutes les communications entre l'orchestre et les services :
// - le tube anonyme pour que l'orchestre envoie des données au service
// - le sémaphore pour que  le service indique à l'orchestre la fin
//   d'un traitement


int creerSemSerOcuper(int numSer);

void creationPipeOrSer(int *pip);

void rendreSemOrSer(int semId);

void prendreSemOrSer(int semId);

int fdSemOrSer();

void detruireSemOrSer(int semId);

int creerSemOrSer();

int fdSemSerComp();

int fdSemSerMax();

int fdSemSerSomme();


#endif
